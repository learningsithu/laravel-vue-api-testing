<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Post</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" type="text/css" media="screen" href="{{asset('/css/app.css')}}" />
    
</head>
<body>
    <div class="container" id="app">
        <div class="h2">Post List</div>
        <post-list></post-list>
    </div>
   
    <script src="{{asset('/js/app.js')}}"></script>
    <script>
        const app = new Vue({
            el: '#app' ,
            method:
        });

    </script>
</body>
</html>