<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\UserResource;
use App\User;
use Carbon\Carbon;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'uid' => $this->id,
            'title' => $this->title,
            'body'  => $this->body,
            'written at' => Carbon::createFromTimeStamp(strtotime($this->created_at))->diffForHumans(),
            'user' => new UserResource(User::find($this->user_id)),
        ];
    }
}
